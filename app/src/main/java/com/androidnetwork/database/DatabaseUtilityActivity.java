package com.androidnetwork.database;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import org.android.dbhelpers.UsersDBHelper;
import org.android.entities.UserEntity;
import org.android.tables.UsersTable;

import java.util.ArrayList;
import java.util.List;

public class DatabaseUtilityActivity extends Activity {

    private TextView textView;
    private UsersDBHelper usersDBHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        textView = (TextView) findViewById(R.id.textview);
        usersDBHelper = new UsersDBHelper(this);

        deleteAllUsers();
        addUsers();
        updateUser("1", new UserEntity("1", "Java", "Gosling"));
        deleteUser("2");
        getUsersList();
    }

    private void deleteAllUsers(){
        usersDBHelper.delete(null);
    }

    private void addUsers() {
        addUser(new UserEntity("1", "James", "Gosling"));
        addUser(new UserEntity("2", "Dennis", "Ritchie"));
        addUser(new UserEntity("3", "Steve", "Jobs"));
    }

    public void addUser(UserEntity userEntity) {
        usersDBHelper.insert(userEntity);
    }

    public void deleteUser(String userId) {
        String where = UsersTable.REQUEST_USER_ID + " = " + userId;
        usersDBHelper.delete(where);
    }

    public void updateUser(String userid, UserEntity userEntity) {
        String where = UsersTable.REQUEST_USER_ID + " = " + userid;
        usersDBHelper.update(userEntity, where);
    }

    public List<UserEntity> getUsersList() {
        ArrayList<UserEntity> userEntities = new ArrayList<>(usersDBHelper.read());
        return userEntities;
    }

}

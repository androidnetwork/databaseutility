package org.android.dbhelpers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.android.contentprovider.BaseSchema;
import org.android.entities.UserEntity;
import org.android.tables.UsersTable;

import java.util.ArrayList;
import java.util.List;

public class UsersDBHelper extends BaseSchema<UserEntity> {

    private ContentResolver contentResolver;

    public UsersDBHelper(Context context) {
        contentResolver = context.getContentResolver();
    }

    @Override
    public long insert(UserEntity entity) {
        try {
            ContentValues contentValues = readContentValues(entity);
            Uri uri = contentResolver.insert(UsersTable.CONTENT_URI,
                    contentValues);
            return ContentUris.parseId(uri);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public int delete(String where) {
        try {
            return contentResolver.delete(UsersTable.CONTENT_URI, where,
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(UserEntity entity, String where) {
        try {
            ContentValues values = readContentValues(entity);
            return contentResolver.update(UsersTable.CONTENT_URI,
                    values, where, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<UserEntity> read() {
        return readData(contentResolver.query(UsersTable.CONTENT_URI,
                null, null, null, null));
    }

    @Override
    public List<UserEntity> read(String where, String sortOrder) {

        return readData(contentResolver.query(UsersTable.CONTENT_URI,
                null, where, null, sortOrder));
    }

    @Override
    public List<UserEntity> readData(Cursor cursor) {
        List<UserEntity> friendItems = new ArrayList<>();

        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    UserEntity info = new UserEntity();

                    info.id = (cursor.getString(cursor
                            .getColumnIndex(UsersTable.REQUEST_ID)));

                    info.userId = (cursor.getString(cursor
                            .getColumnIndex(UsersTable.REQUEST_USER_ID)));


                    info.firstName = (cursor.getString(cursor
                            .getColumnIndex(UsersTable.REQUEST_FIRSTNAME)));

                    info.lastName = (cursor.getString(cursor
                            .getColumnIndex(UsersTable.REQUEST_LASTNAME)));


                    friendItems.add(info);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (cursor != null && !cursor.isClosed())
            cursor.close();

        return friendItems;
    }

    @Override
    public ContentValues readContentValues(UserEntity entity) {
        ContentValues contentValues = new ContentValues();

        try {
            contentValues.put(UsersTable.REQUEST_USER_ID, entity.userId);
            contentValues.put(UsersTable.REQUEST_FIRSTNAME, entity.firstName);
            contentValues.put(UsersTable.REQUEST_LASTNAME, entity.lastName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentValues;
    }

    @Override
    public int getRowCount(Uri uri, ContentResolver contentResolver) {
        return super.getRowCount(UsersTable.CONTENT_URI, this.contentResolver);
    }
}

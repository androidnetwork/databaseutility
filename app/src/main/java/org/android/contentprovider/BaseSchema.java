package org.android.contentprovider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import java.util.List;

public abstract class BaseSchema<T> {
    public abstract List<T> readData(Cursor cursor);

    public abstract ContentValues readContentValues(T entity);

    public abstract long insert(T entity);

    public abstract int delete(String where);

    public abstract int update(T entity, String where);

    public abstract List<T> read();

    public abstract List<T> read(String where,String sortOrder);

    /************************************************
     * Utils
     **********************************************************************/

    public int getRowCount(Uri uri, ContentResolver contentResolver) {
        int count = 0;
        try {
            Cursor cursor = contentResolver.query(uri,
                    new String[]{"count(*)"}, null, null, null);

            if (cursor.getCount() == 0) {
                cursor.close();
                count = 0;
            } else {
                cursor.moveToFirst();
                count = cursor.getInt(0);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
}

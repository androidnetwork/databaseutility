package org.android.contentprovider;

import android.net.Uri;

/**
 * Entity class to get details of the database 
 * 
 * @author AndroidNetwork
 *
 */
public class DBInfo {
	public static final String PARAMETER_NOTIFY = "notify";
	public static final String DATABASE_NAME = "user.sqlite";
	public static final int DATABASE_VERSION = 1;
	public static final String AUTHORITY = "org.android.contentprovider";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
}

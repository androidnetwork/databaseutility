package org.android.tables;

import android.net.Uri;

import org.android.contentprovider.DBInfo;


public class UsersTable {

    public static String TABLENAME = "user";

    public static final String REQUEST_ID = "_id";

    public static final String REQUEST_USER_ID = "user_id";

    public static final String REQUEST_FIRSTNAME = "firstname";

    public static final String REQUEST_LASTNAME = "lastname";

    public static final Uri CONTENT_URI = Uri.parse("content://" + DBInfo.AUTHORITY + "/" + TABLENAME + "?" + DBInfo.PARAMETER_NOTIFY + "=true");
}
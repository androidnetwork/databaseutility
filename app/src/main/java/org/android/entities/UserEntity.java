package org.android.entities;

public class UserEntity {

    public String id = "", firstName = "", lastName = "", userId = "";

    public UserEntity(String userId, String firstName, String lastName) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserEntity() {

    }
}
